const express = require("express");
const cors = require("cors");
const User = require("./model/users");
const { compare, hash } = require("bcryptjs");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const { verify } = require("jsonwebtoken");
const fs = require("fs");

const envExists = fs.existsSync(".env");

require("dotenv").config({ path: envExists ? ".env" : ".env.example" });

const {
  createAccessToken,
  isAuth,
  createRefreshToken,
  sendRefreshToken,
} = require("./auth");

const app = express();

app.use(
  cors({
    credentials: true,
    origin: "http://localhost:3000",
  })
);

app.use(cookieParser());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post("/login", async (req, res) => {
  try {
    const user = await User.findByEmail(req.body.email);

    if (!user) throw new Error("User does not exist");

    const isValid = await compare(req.body.password, user.password);

    if (!isValid) throw new Error("Passwords do not match");

    const refreshToken = createRefreshToken(user);
    sendRefreshToken(res, refreshToken);

    return res.json({
      accessToken: createAccessToken(user),
      user: user.email,
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      error: err.message,
    });
  }
});

app.post("/logout", (req, res) => {
  sendRefreshToken(res, "");

  return res.json({
    accessToken: "",
  });
});

app.post("/refresh_token", async (req, res) => {
  const token = req.cookies.eid;

  let payload = null;
  try {
    if (!token) throw new Error("Token not found");

    payload = verify(token, process.env.REFRESH_TOKEN_SECRET);

    const user = await User.findByEmail(payload.email);

    if (!user) throw new Error("User not found");

    const refreshToken = createRefreshToken(user);
    sendRefreshToken(res, refreshToken);

    return res.send({ ok: true, accessToken: refreshToken });
  } catch (err) {
    console.log(err);
    return res.status(400).send({ ok: false, accessToken: "" });
  }
});

app.post("/register", async (req, res) => {
  try {
    const hashedPassword = await hash(req.body.password, 12);

    const exists = await User.findByEmail(req.body.email);

    if (exists) throw new Error("User already exists");

    await User.create({
      email: req.body.email,
      password: hashedPassword,
    });

    const refreshToken = createRefreshToken(req.body);
    sendRefreshToken(res, refreshToken);

    return res.status(201).json({
      message: "User created",
      accessToken: refreshToken,
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({ error: err.message });
  }
});

app.listen(3002, () => {
  console.log("listening on 3002");
});
