const { sign, verify } = require("jsonwebtoken");

exports.createAccessToken = (user) => {
  return sign({ email: user.email }, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: "3h",
  });
};

exports.createRefreshToken = (user) => {
  return sign({ email: user.email }, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: "3d",
  });
};

exports.isAuth = ({ context }, next) => {
  try {
    const authorization = context.req.headers["authorization"];

    if (!authorization || !authorization.includes("Bearer"))
      throw new Error("No authorization token");

    const token = authorization.split(" ")[1];

    const payload = verify(token, process.env.ACCESS_TOKEN_SECRET);

    context.payload = payload;

    return next();
  } catch (err) {
    console.log(err);

    return null;
  }
};

exports.sendRefreshToken = (res, token) => {
  return res.cookie("eid", token, {
    httpOnly: true,
    sameSite: "strict",
    secure: true,
    path: "/refresh_token",
  });
};
