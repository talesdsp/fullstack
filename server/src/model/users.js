const { writeFile } = require("../utils");
const users = require("../data/users.json");

function findByEmail(email) {
  return new Promise((resolve, _) => {
    const user = users.find((p) => p && p.email === email);

    resolve(user);
  });
}

function create(data) {
  return new Promise((resolve, _) => {
    users.push(data);
    writeFile("./src/data/users.json", users);

    resolve(data);
  });
}

module.exports = {
  findByEmail,
  create,
};
