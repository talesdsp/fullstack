# Client

### Requisitos

Sua máquina deve ter o nodejs instalado.

### Como iniciar

_A fim de exemplo utilizarei o `yarn` nos comandos a seguir, mas pode ser substituído por `npm`_.

Primeiro instale todas as dependências com `yarn install`.

Para o modo de desenvolvimento rode somente:

- `yarn start`

Para o modo de produção rode:

- `yarn build`

- `yarn global add serve` (instale o `serve` globalmente)

- `serve -s build`
