import { Router } from "@reach/router";
import React from "react";
import Home from "./pages/Home/Home";
import Login from "./pages/Login/Login";
import Register from "./pages/Register/Register";
import Template from "./template/Template";

function App() {
  return (
    <Router>
      <Template path="/">
        <Home path="/" />
      </Template>

      <Login path="login" />
      <Register path="register" />
    </Router>
  );
}

export default App;
