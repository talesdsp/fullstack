import React, { useState } from "react";
import { Main, Nav, Sidebar } from "./components";

const Template = ({ children }) => {
  const [open, setOpen] = useState(false);

  const toggleBurger = () => {
    const burger = document.querySelector("#burger");
    const classes = burger.classList;

    classes.toggle("open");
    setOpen((prev) => !prev);
  };

  const handleClick = (e) => {
    const el = document.querySelector(".active");

    if (!el) return e.target.parentElement.classList.add("active");

    el.classList.remove("active");

    const isSame = e.target.parentElement.classList === el.classList;

    if (isSame) return;

    e.target.parentElement.classList.add("active");
  };

  return (
    <>
      <Nav setOpen={setOpen} toggleBurger={toggleBurger} />
      <Sidebar open={open} handleClick={handleClick} />
      <Main children={children} />
    </>
  );
};

export default Template;
