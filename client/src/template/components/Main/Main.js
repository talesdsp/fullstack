import styled from "@emotion/styled";
import React from "react";

const __Main = styled.main`
  padding-top: 120px;
  margin-left: 0px;

  @media (min-width: 1280px) {
    margin-left: 255px;
  }

  @media (min-width: 1920px) {
    margin-left: 345px;
  }
`;

const Main = ({ children }) => {
  return <__Main children={children} />;
};

export default Main;
