import React from "react";
import { images, svgs } from "../../../assets";
import "./sidebar.css";

const Sidebar = ({ open, handleClick }) => {
  return (
    <aside className={`sidebar ${open ? "open" : ""}`}>
      <ul>
        <li className="active">
          <div className="row" onClick={handleClick}>
            <img src={svgs.dashboard} alt="" />
            Painel
          </div>
          <ul>
            <li>Salas</li>
            <li>Temas</li>
            <li>Palestrantes</li>
          </ul>
        </li>
        <li>
          <div className="row" onClick={handleClick}>
            <img src={svgs.smallCalendar} alt="" />
            Programação
          </div>
          <ul>
            <li>Salas</li>
            <li>Temas</li>
            <li>Palestrantes</li>
          </ul>
        </li>
        <li>
          <div className="row" onClick={handleClick}>
            <img src={svgs.avatar} alt="" />
            Participantes
          </div>
          <ul>
            <li>Salas</li>
            <li>Temas</li>
            <li>Palestrantes</li>
          </ul>
        </li>
        <li>
          <div className="row" onClick={handleClick}>
            <img src={svgs.theater} alt="" />
            Vouchers
          </div>
          <ul>
            <li>Salas</li>
            <li>Temas</li>
            <li>Palestrantes</li>
          </ul>
        </li>
        <li>
          <div className="row" onClick={handleClick}>
            <img src={svgs.setting} alt="" />
            Preferências
          </div>
          <ul>
            <li>Salas</li>
            <li>Temas</li>
            <li>Palestrantes</li>
          </ul>
        </li>
      </ul>

      <img src={images.logo} alt="" />

      <span>
        Desenvolvido por <br /> Deploy. Future Agency
      </span>
    </aside>
  );
};

export default Sidebar;
