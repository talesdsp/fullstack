import styled from "@emotion/styled";

const __Burger = styled.span`
  position: relative;

  &,
  ::before,
  ::after {
    background-color: #000;
    width: 20.35px;
    height: 2.35px;
    border-radius: 8px;
    transition: transform 400ms ease;
  }

  ::before,
  ::after {
    position: absolute;
    content: "";
    left: 0px;
  }

  ::before {
    top: -6px;
  }
  ::after {
    bottom: -6px;
  }

  &.open {
    transform: translateX(8px);

    ::before,
    ::after {
      transform: translateX(-8px);
    }
  }

  @media (min-width: 1280px) {
    transform: translateX(8px);

    ::before,
    ::after {
      transform: translateX(-8px);
    }
  }
`;

const __Button = styled.button`
  width: 30px;
  height: 30px;
  position: relative;
  display: grid;
  align-items: center;
  justify-content: center;
  background: transparent;
  margin: auto 20px;

  @media (min-width: 1280px) {
    pointer-events: none;
  }

  @media (min-width: 1920px) {
    margin-left: 50px;
    margin-right: 40px;
  }
`;

const Hamburger = ({ toggleBurger }) => {
  return (
    <__Button onClick={toggleBurger}>
      <__Burger id="burger" />
    </__Button>
  );
};

export default Hamburger;
