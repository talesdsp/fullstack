import { css, keyframes } from "@emotion/core";
import styled from "@emotion/styled";

const fadeUp = keyframes`
0%, 100%{
  transform: translateY(100%);
  opacity: 0;
}

20%, 80%{
  transform: translateY(-160%);
  opacity: 1;
}

`;

export const __Nav = styled.nav`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  background-color: #fff;
  height: 90px;
  position: fixed;
  z-index: 50;

  @media (min-width: 1280px) {
    height: 120px;
  }

  .image-wrapper {
    display: none;
    @media (min-width: 1280px) {
      display: block;
      width: 255px;
    }

    @media (min-width: 1920px) {
      width: 345px;
    }

    img {
      width: 100%;
      display: block;
      margin: auto;
      cursor: pointer;

      @media (min-width: 1280px) {
        width: 235px;
      }
    }
  }

  > span {
    animation: ${fadeUp} 5s forwards ease;
    position: fixed;
    display: block;
    width: fit-content;
    padding: 5px 10px;
    border-radius: 20px;
    bottom: 0;
    left: 0;
    right: 0;
    font-weight: 500;
    color: #fff;
    background-color: #000000aa;
    font-size: 16px;
    margin: auto;

    @media (min-width: 1280px) {
      color: #000;
      animation: unset;
      padding: unset;
      background-color: unset;
      border-radius: unset;
      font-weight: 600;
      font-size: 30px;
      position: relative;
    }
  }

  .input {
    margin: auto;

    @media (min-width: 1280px) {
      margin-right: 2px;
    }
  }

  .dropdown {
    transition: transform 200ms ease;
    transform: scale(0);
    transform-origin: 80% top;
    position: fixed;
    top: 90px;
    right: 0;
    background: #000000aa;
    padding: 10px;
    color: #fff;

    &.open {
      transform: scale(1);
    }

    ::after {
      position: absolute;
      content: "";
      display: block;
      margin: auto;
      right: 20px;
      top: -15px;
      width: 0;
      height: 0;
      border-right: 12px solid transparent;
      border-left: 12px solid transparent;
      border-bottom: 15px solid #000000aa;
    }

    li {
      position: relative;
      padding: 5px 0;
      cursor: pointer;

      &:nth-child(1) {
        border-bottom: 1px solid #ddd;

        &::after {
          content: "";
          border: 2px solid #fff;
          border-radius: 50%;
          position: absolute;
          width: 6px;
          height: 6px;
          top: 0;
          right: 0;
          background: #fe634e;
        }
      }
    }
  }
`;

export const __Separator = styled.span`
  width: 1px;
  margin: auto 5px;
  height: 56px;
  background-color: rgba(0, 0, 0, 0.03);
`;

export const __Buttons = styled.div`
  margin: auto;
  display: none;
  margin-right: 20px;
  flex-direction: row;
  align-items: center;

  @media (min-width: 1280px) {
    display: flex;
  }

  @media (min-width: 1920px) {
    margin-right: 40px;
  }
`;

export const __Button = styled.button`
  width: 56px;
  height: 56px;
  border-radius: 20px;
  margin: 0 8px;
  position: relative;

  ${(props) =>
    props.notify &&
    css`
      &::after {
        content: "";
        border: 2px solid #fff;
        border-radius: 50%;
        position: absolute;
        width: 16px;
        height: 16px;
        top: -5px;
        right: -5px;
        background: #fe634e;
      }
    `}

  @media(min-width: 1920px) {
    margin: 0 15px;
  }

  ${(props) =>
    props.mobile &&
    css`
      margin-right: 10px;
      @media (min-width: 1280px) {
        display: none;
      }
    `}
`;
