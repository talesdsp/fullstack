import React from "react";
import { images, svgs } from "../../../assets";
import Input from "../../../components/Input";
import Hamburger from "./Hamburger";
import { __Button, __Buttons, __Nav, __Separator } from "./nav.styled";

const Nav = ({ toggleBurger }) => {
  const toggleDropdown = () => {
    document.querySelector(".dropdown").classList.toggle("open");
  };

  return (
    <__Nav>
      <div className="image-wrapper">
        <img src={images.logo} alt="example logo" />
      </div>

      <Hamburger toggleBurger={toggleBurger} />
      <span>Olá, Gabriel Teixeira</span>

      <div className="input">
        <Input asset={svgs.search} />
      </div>

      <__Buttons>
        <__Button notify children={<img src={svgs.bell} alt="bell" />} />

        <__Button notify children={<img src={svgs.chat} alt="chat" />} />

        <__Separator />

        <__Button children={<img src={images.user} alt="user" />} />
      </__Buttons>

      <__Button
        onClick={toggleDropdown}
        notify
        mobile
        children={<img src={images.user} alt="user" />}
      />

      <ul className="dropdown">
        <li>Notificações</li>
        <li>Mensagens</li>
      </ul>
    </__Nav>
  );
};

export default Nav;
