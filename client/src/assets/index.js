import chatBubble from "./images/chat.png";
import logo from "./images/logo.png";
import user from "./images/user.png";
import bell from "./svgs/ic_bell.svg";
import bellLarge from "./svgs/ic_bell_large.svg";
import calendar from "./svgs/ic_calendar.svg";
import smallCalendar from "./svgs/ic_calendar_small.svg";
import chat from "./svgs/ic_chat.svg";
import dashboard from "./svgs/ic_dashboard.svg";
import search from "./svgs/ic_search.svg";
import setting from "./svgs/ic_setting.svg";
import theater from "./svgs/ic_theater.svg";
import avatar from "./svgs/ic_user.svg";
import wave from "./svgs/wave.svg";
import wave2 from "./svgs/wave2.svg";

export const images = {
  logo,
  user,
  chatBubble,
};

export const svgs = {
  bell,
  bellLarge,
  wave,
  wave2,
  calendar,
  chat,
  dashboard,
  search,
  setting,
  smallCalendar,
  theater,
  avatar,
};
