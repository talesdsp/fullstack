function storeToken() {
  let accessToken;

  const getAccessToken = () => accessToken;
  const setAccessToken = (token) => (accessToken = token);

  return {
    getAccessToken,
    setAccessToken,
  };
}

export const { getAccessToken, setAccessToken } = storeToken();
