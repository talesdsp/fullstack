import { navigate } from "@reach/router";
import React, { useEffect } from "react";
import { setAccessToken } from "../../access-token";
import { images } from "../../assets";
import Notification from "../../components/Notification";
import EventClasses from "./components/EventClasses";
import HeroCard from "./components/HeroCard";
import Instagram from "./components/Instagram";
import Mural from "./components/Mural";
import "./home.css";

const Home = ({}) => {
  useEffect(() => {
    fetch("http://localhost:3002/refresh_token", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      credentials: "include",
    })
      .then(async (res) => {
        const { ok, accessToken } = await res.json();

        console.log(ok, accessToken);

        if (!ok) throw new Error("No token");

        setAccessToken(accessToken);
      })
      .catch((err) => {
        console.log(err);
        navigate("/login");
      });
  }, []);

  return (
    <div className="home">
      <Notification />
      <HeroCard />

      <div className="row-column">
        <div className="column">
          <EventClasses />
          <Instagram />
        </div>
        <Mural />
      </div>

      <img className="chat-bubble" src={images.chatBubble} alt="" />
    </div>
  );
};

export default Home;
