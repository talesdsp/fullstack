import React from "react";
import "./event-classes.css";

const EventClasses = () => {
  return (
    <div className="event-classes">
      <div className="header">
        <h2 className="title">Salas do Evento</h2>

        <div className="paginate">
          <button className="button disabled">
            <span className="arrow-left" />
          </button>
          <span className="dot active" />
          <span className="dot" />
          <span className="dot" />
          <span className="dot" />
          <span className="dot" />
          <button className="button">
            <span className="arrow-right" />
          </button>
        </div>
      </div>

      <div className="cards">
        {[...Array(3)].map((event, i) => (
          <div key={i} className="card">
            <div className="header">
              <img src="" alt="" />
            </div>
            <div className="content">
              <div className="title">
                Novos produtos e serviços para esse momento
              </div>
              <div className="tags">#vendas #produtos #serviços</div>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad mini
              </p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default EventClasses;
