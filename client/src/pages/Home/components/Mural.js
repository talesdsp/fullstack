import React from "react";
import "./mural.css";

const Mural = () => {
  return (
    <div className="mural">
      <div className="header">
        <h4>Mural do Evento</h4>
      </div>

      {[...Array(3)].map((item, i) => (
        <div key={i} className="item">
          <div className="header">
            <figure>
              <img src="" alt="" />
            </figure>

            <div className="header-text">
              <h5>Nome Exemplo 1</h5>
              <h6>😍😁😢😡</h6>
              <span className="timestamp">5m atrás</span>
            </div>
          </div>

          <p className="content">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad mini. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </p>
        </div>
      ))}

      <button className="button">Publicar no mural</button>
    </div>
  );
};

export default Mural;
