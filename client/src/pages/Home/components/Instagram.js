import React from "react";
import "./instagram.css";

const Instagram = () => {
  return (
    <div className="instagram">
      <div className="header">
        <h2 className="title">Instagram</h2>
        <h2 className="subtitle">
          Utilize a hashtag #convencao2021 para ver a sua foto no nosso mural.
        </h2>
      </div>
      <div className="photos">
        <img src="" alt="" />
        <img src="" alt="" />
        <img src="" alt="" />
        <img src="" alt="" />
        <img src="" alt="" />
        <img src="" alt="" />
        <img src="" alt="" />
        <img src="" alt="" />
      </div>
    </div>
  );
};

export default Instagram;
