import React from "react";
import { svgs } from "../../../assets";
import "./hero-card.css";

const HeroCard = () => {
  return (
    <div className="hero-card">
      <img />
      <div className="header">
        <div className="headings">
          <h2>Convenção 2021</h2>
          <h3>Comunicação e Vendas</h3>
        </div>

        <div className="calendar">
          <img src={svgs.calendar} alt="" />

          <div className="dates">
            <span>Início: 14.01.2021</span>
            <span>Término: 20.01.2021</span>
          </div>
        </div>
      </div>

      <div className="info">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          mini. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          mini. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          mini. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          mini.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          mini.
        </p>
      </div>
    </div>
  );
};

export default HeroCard;
