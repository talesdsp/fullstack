import { Link, navigate } from "@reach/router";
import React from "react";
import { setAccessToken } from "../../access-token";
import { images, svgs } from "../../assets";
import Input from "../../components/Input";

const Register = () => {
  async function handleSubmit(event) {
    event.preventDefault();
    try {
      const email = document.querySelector('input[type="email"]');
      const password = document.querySelector('input[type="password"]');

      const res = await fetch("http://localhost:3002/register", {
        body: JSON.stringify({
          email: email.value,
          password: password.value,
        }),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        credentials: "include",
        method: "POST",
      });

      const { accessToken } = await res.json();

      setAccessToken(accessToken);

      navigate("/");
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div className="wrapper">
      <form className="card-form" onSubmit={handleSubmit}>
        <h1>Register</h1>
        <Input label="email" placeholder="email" type="email" />
        <br />
        <Input type="password" label="password" placeholder="password" />
        <br />
        <button type="submit">submit</button>
        <Link to="/login">Já tem conta?</Link>
        <Link to="/">
          <img src={images.logo} alt="" />
        </Link>
      </form>
      <img src={svgs.wave} alt="" className="wave-bottom" />
    </div>
  );
};

export default Register;
