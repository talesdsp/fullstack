import styled from "@emotion/styled";

const __container = styled.div`
  background-color: #fbfbfb;
  border: 1px solid #efefef;
  padding: 0 15px;
  width: 100%;
  height: 56px;
  border-radius: 20px;
  display: flex;
  flex-direction: row;
  align-items: center;

  input {
    background-color: transparent;
    border: none;
    font-size: 16px;
    font-weight: 400;
    width: 100%;
    padding: 20px 0;
    outline: none;

    ::placeholder {
      color: #b9b9b9;
    }
  }

  @media (min-width: 768px) {
    width: 50vw;
    max-width: 100%;
  }

  @media (min-width: 1280px) {
    width: 414px;
  }
`;

export default ({
  reference,
  label,
  asset,
  placeholder = "Procurar...",
  type = "text",
}) => {
  return (
    <__container>
      <input
        ref={reference}
        aria-label={label}
        type={type}
        placeholder={placeholder}
      />
      {asset && <img src={asset} />}
    </__container>
  );
};
