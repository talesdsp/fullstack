import { keyframes } from "@emotion/core";
import styled from "@emotion/styled";
import { svgs } from "../assets";

const fadeDown = keyframes`
0%, 100%{
  transform: translateY(-160px);
  opacity: 0;
}

10%, 90%{
  transform: translateY(0px);
  opacity: 1;
}
`;

const __wrapper = styled.div`
  display: block;
  position: fixed;
  width: 332px;
  max-width: 90%;
  height: 119px;
  border-radius: 20px;
  animation: ${fadeDown} 20000ms ease infinite;
  color: #fff;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: #189cbe;
  padding: 20px;
  padding-bottom: 15px;
  top: 100px;
  font-weight: bold;
  font-size: 14px;
  margin: auto;
  right: 0;
  left: 0;
  z-index: 100;

  @media (min-width: 768px) {
    padding-bottom: 20px;
    left: unset;
    height: 149px;
    font-size: 18px;
    right: 50px;
  }

  @media (min-width: 1280px) {
    top: 165px;
  }
`;

const __row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 10px;

  img {
    margin-right: 10px;
  }
`;

const __button = styled.button`
  background-color: #f5a737;
  font-size: 16px;
  font-weight: bold;
  color: #fff;
  height: 40px;
  width: 210px;
  border-radius: 20px;
`;

const Notification = () => {
  return (
    <__wrapper>
      <__row>
        <img src={svgs.bellLarge} alt="" />A Palestra Lorem Ipsum vai começar.
      </__row>
      <__button>Ir para a sala</__button>
    </__wrapper>
  );
};

export default Notification;
